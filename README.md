The most simple and intuitive Script for securely storing your Player Preferences!

#### Features

- Store any Key-Value pair
- Same interface as Unity PlayerPrefs
- Values are encrypted (Rijndael)
- Keys are hashed

#### Tested platforms

- Windows
- Mac
- Android & iOS
- WebGL

#### Examples

```
SPrefs.SetInt(key, 7)
SPrefs.GetInt(key)
SPrefs.GetString(key, "defaultValue")
SPrefs.SetBool(key, true)
```

The script comes with an example class and a test scene. If you want to publish your project, make sure to change the ENCRYPTION_KEY to a different hash (see Cryptor class).
